# sisop-praktikum-modul-4-2023-HS-E11



## Anggota Kelompok E11
| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Febriansyah      | 5025211164 |
| Sastiara Maulikh          | 5025201257 |
| Schaquille Devlin Aristano| 5025211211 | 


# Soal 1
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

# Catatan
- Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
- Perhatikan port  pada masing-masing instance.

## Soal 1 bagian a
Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

```kaggle datasets download -d bryanb/fifa-player-stats-database```

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 


## Cara Penyelesaian`
Dilakukan konfigurasi terlebih dahulu dengan membuat akun kaggle lalu mengambil API KEY dan di hubungkan kedalam folder yang berisi ```storage.c``` dengan format kangle.json lalu ketika kaggle sudah berhasil di konfigurasi dilakukan dowload dan unzip dengan perintah ```execlp```

## Source Code
```sh

    pid_t pid;
    int status;

    pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execlp("kaggle", "kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    }
    waitpid(pid, &status, 0);

    pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execlp("unzip", "unzip", "fifa-player-stats-database.zip", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    }
    waitpid(pid, &status, 0);

```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/06/Screenshot-117-e1685797614668.png)

## Soal 1 bagian b
Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.


## Cara Penyelesaian
Dalam file storage.c, dibuka file CSV dengan mode "r" (read) menggunakan fungsi ```fopen``` pada file FIFA23_official_data. Kemudian, kita digunakan array line untuk menyimpan setiap baris dari file CSV.

lalu dilakukan loop while, untuk membaca setiap baris dari file CSV menggunakan fungsi fgets.
Kemudian, kita memeriksa jumlah kolom yang cukup dan mengekstrak nilai-nilai yang relevan dari kolom-kolom tersebut, seperti nama pemain, klub, umur, potensi, URL foto, kebangsaan, harga, gaji, dan lainnya.

Selanjutnya, output dibatasi dengan kriteria yang diminta dan jika pemain memenuhi kondisi yang sudah ditentukan, maka akan mencetak informasi pemain menggunakan fungsi printf. dan dilakukan Loop hingga mencapai akhir file.

## Source Code
```sh

    if (file == NULL) {
    FILE *file = fopen("FIFA23_official_data.csv", "r");
        perror("Failed to open the CSV file");
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    int line_count = 0;
    while (fgets(line, sizeof(line), file)) {
        if (line_count > 0) {
            char *field;
            char *fields[MAX_FIELDS];
            int field_count = 0;

            // Split the line by comma delimiter
            field = strtok(line, ",");
            while (field != NULL && field_count < MAX_FIELDS) {
                fields[field_count++] = field;
                field = strtok(NULL, ",");
            }

            if (field_count >= 12) {
                // Extract relevant fields
                char *name = fields[1];
                char *club = fields[8];
                int age = atoi(fields[2]);
                int potential = atoi(fields[7]);
                char *photo_url = fields[5];
                char *nationality = fields[4];
                char *price = fields[10];
                char *wage = fields[11];

                // Check conditions
                if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0) {
                    printf("---------------------------------------\n");
                    printf("Nama: %s\n", name);
                    printf("Club: %s\n", club);
                    printf("Umur: %d\n", age);
                    printf("Potensi: %d\n", potential);
                    printf("URL Foto: %s\n", photo_url);
                    printf("Kebangsaan: %s\n", nationality);
                    printf("Harga: %s\n", price);
                    printf("Gaji: %s\n", wage);
                    printf("---------------------------------------\n");
                }
            }
        }

        line_count++;
    }

    fclose(file);

```

### hasil output
[list output pemain](https://drive.google.com/file/d/184q6LLOx7xLdSRkzPUcgDcsfiQ6o4btt/view?usp=sharing)

## Soal 1 bagian c
Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

## Cara Penyelesaian`
Dibuat Dockerfile untuk menjalankan storage.c dengan pada dockerfile dilakukan dowload untuk segala kebutuhan dalam menjalankan storage.c seperti  gcc,unzip,python3-pip, dll. lalu dijalankan perintah ```docker build -t storage . ``` untuk membuat docker image lalu di run dengan perintah  ``` docker run storage ```

## Source Code
```sh
# Base image
FROM ubuntu:latest

# Install dependencies
RUN apt-get update && apt-get install -y \
    gcc \
    make \
    unzip \
    python3 \
    python3-pip \
    libc6-dev

# Install Kaggle CLI
RUN pip3 install kaggle

COPY storage.c /app/
WORKDIR /app
RUN gcc -o storage storage.c
CMD ["./storage"]


```

## Soal 1 bagian d
Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.


## Cara Penyelesaian
setelah berhasil dibuat docker file, maka akan dilakukan push ke akun docker yang sudah di konfigurasikan sebelumnya akun saya bernama riansyah20.
dijalankan ```docker tag storage riansyah20/storage-app``` untuk menghubungkan docker image kita
dilakukan ```docker push riansyah20/storage-app``` untuk mengupload ke akun docker secara publik


### link docker
[link docker](https://hub.docker.com/r/riansyah20/storage-app)


## Soal 1 bagian E
Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.


## Cara Penyelesaian
dibuat folder baru bernama Barcelona dan Napoli lalu terminal masuk kedalam dan membuat file docker-compose.yml dengan perintah ```touch```. kemudian dilakukan ```replicas``` untuk menjalankan 5 instance dari layanan yang ditentukan di setiap folder. dan terakhir dilakukan perintah ```docker-compose up -d```

## Source Code
```sh
#Barcelona folder
version: '3'

services:
  Barcelona:
    image: riansyah20/storage-app
    deploy:
      replicas: 5


#Napoli Folder
version: '3'

services:
  Napoli:
    image: riansyah20/storage-app
    deploy:
      replicas: 5


```

### hasil output
![image](http://itspreneur.com/wp-content/uploads/2023/06/Screenshot-118-e1685799338788.png)


# Soal 2
Nana adalah peri kucing yang terampil dalam menggunakan sihir, dia bisa membuat orang lain berubah menjadi Molina. Suatu hari, dia bosan menggunakan sihir dan mencoba untuk magang di germa.dev sebagai programmer.
Agar dapat diterima sebagai karyawan magang dia diberi sebuah file .zip yang berisi folder dan file dari Germa. Kemudian, Nana harus membuat sistem manajemen folder dengan ketentuan sebagai berikut:
Apabila terdapat file yang mengandung kata restricted, maka file tersebut tidak dapat di-rename ataupun dihapus.
Apabila terdapat folder yang mengandung kata restricted, maka folder tersebut, folder yang ada di dalamnya, dan file yang ada di dalamnya tidak dapat di-rename ataupun dihapus.
Contoh:
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kane.txt

Karena latar belakang Nana adalah seorang penyihir, dia ingin membuat satu kata sihir yaitu bypass yang dapat menghilangkan kedua aturan tadi. Kata sihir tersebut akan berlaku untuk folder dan file yang ada di dalamnya.
Case invalid untuk bypass:
/jalan/keputih/perintis/iv/tidakrestrictedlohini/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gangIV/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanelagiramai.txt
Case valid untuk bypass:
/jalan/keputih/perintis/iv/tadirestrictedtapibypasskok/gang/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kane.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/bypassbanget/kaneudahtutup.txt
/jalan/keputih/perintis/iv/sangatrestrictedloh/gang/kanepakaibypass.txt

Setelah Nana paham aturan tersebut, bantu Nana untuk membuat sebuah FUSE yang bernama germa.c, yang mana dapat melakukan make folder, rename file dan folder, serta delete file dan folder. 


# Catatan
Keterangan untuk file .zip.
Folder src_data adalah folder source FUSE.
Folder dest_data adalah folder destination FUSE.

## uji perintah
 Untuk mengujinya, maka lakukan hal-hal berikut:
Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Kemudian, buatlah folder projectMagang pada /src_data/germa/projects/restricted_list/. Akan tetapi, hal tersebut akan gagal.
Ubahlah nama folder dari restricted_list pada folder /src_data/germa/projects/restricted_list/ menjadi /src_data/germa/projects/bypass_list/. Kemudian, buat folder projectMagang di dalamnya.
Karena folder projects menjadi dapat diakses, ubahlah folder filePenting di dalam folder projects menjadi restrictedFilePenting agar secure kembali. Coba keamanannya dengan mengubah nama file yang ada di dalamnya.
Terakhir, coba kamu hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus
Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 
Adapun format logging yang harus kamu buat adalah sebagai berikut. 
[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]
Dengan keterangan sebagai berikut.
STATUS: SUCCESS atau FAILED.
dd: 2 digit tanggal.
MM: 2 digit bulan.
yyyy: 4 digit tahun.
HH: 24-hour clock hour, with a leading 0 (e.g. 22).
mm: 2 digit menit.
ss: 2 digit detik.
CMD: command yang digunakan (MKDIR atau RENAME atau RMDIR atau RMFILE atau lainnya).
DESC: keterangan action, yaitu:
[User]-Create directory x.
[User]-Rename from x to y.
[User]-Remove directory x.
[User]-Remove file x.

Contoh:
SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y

## Cara Penyelesaian`
dibuat program dengan mengimplementasikan FUSE untuk membuat file system virtual yang menyediakan operasi-operasi file system seperti membaca atribut & isi file, membaca direktori, Membuat direktori,Menghapus direktori, menghapus file, membuat file, mengganti nama file. dimana untuk setiap perintah akan tercatat pada logmucatatsini.txt dengan format yang sudah diberikan.

pejelasan program :

- versi FUSE yang digunakan dengan #define FUSE_USE_VERSION 28.

- Terdapat fungsi status_log yang digunakan untuk mencatat log ke file logmucatatsini.txt. Fungsi ini menerima argumen seperti kategori log, waktu, operasi yang dilakukan, dan deskripsi tambahan.

- Terdapat variabel direktori yang menunjukkan direktori tempat file system virtual ini berada(/home/riansyah/praktikum4/soal2/nanaxgerma/src_data) (harap disesuaikan).

- Terdapat fungsi-fungsi seperti germa_getattr, germa_read, germa_readdir, dan lainnya yang mengimplementasikan operasi-operasi file system seperti membaca atribut file, membaca isi file, membaca direktori, membuat direktori, menghapus direktori, menghapus file, membuat file, dan mengganti nama file, .

- Fungsi isRestricted digunakan untuk memeriksa apakah suatu path terlarang atau tidak. Jika suatu path mengandung kata "restricted", maka path tersebut dianggap terlarang. namun jika ada kata "bypass" maka path tidak terlarang

- Fungsi-fungsi operasi file system seperti germa_getattr, germa_read, dan lainnya mengakses file atau direktori pada path yang diberikan dan melakukan operasi yang sesuai. Jika operasi berhasil, fungsi status_log dipanggil untuk mencatat log ke file logmucatatsini.txt.

- pada fungsi main, program mengatur umask menjadi 0 untuk menghindari masalah izin saat membuat file atau direktori.

Program kemudian memanggil fungsi fuse_main untuk menjalankan file system virtual ini dengan menggunakan fungsi dan operasi yang telah ditentukan sebelumnya.

## Output
![image](http://itspreneur.com/wp-content/uploads/2023/06/Screenshot-122-e1685803284597.png)

## Source Code
```sh
#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>
#include <fuse.h>
#include <errno.h>

void status_log(const char *temp, const char *operation, const char *desc, const char *add)
{
time_t now;
time(&now);
struct tm *timestamp = localtime(&now);
FILE *logging = fopen("/home/riansyah/praktikum4/soal2/logmucatatsini.txt", "a");

char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

if (logging != NULL) {
    fprintf(logging, "%s::%s::%s::riansyah-", temp, clock, operation);

            if (strcmp(operation, "MKDIR") == 0) {
            fprintf(logging, "Create directory %s\n", desc);
        } else if (strcmp(operation, "RMDIR") == 0) {
            fprintf(logging, "Remove directory %s\n", desc);
        } else if (strcmp(operation, "RENAME") == 0) {
            fprintf(logging, "Rename from %s to %s\n", desc,add);
        } else if (strcmp(operation, "TOUCH") == 0) {
            fprintf(logging, "Created file %s\n", desc);
        }else if (strcmp(operation, "RMFILE") == 0) {
            fprintf(logging, "Remove file %s\n", desc);
        } 

        fclose(logging);
    }
}

const char *direktori = "/home/riansyah/praktikum4/soal2/nanaxgerma/src_data";

static bool isRestricted(const char *path) {
    bool check = false;
    char *cp = strdup(path);
    char *link = strtok(cp, "/");

    while (link != NULL) {
        if (strstr(link, "bypass") != NULL) {
            check = false;
            break;
        }
        if (strstr(link, "restricted") != NULL) {
            check = true;
            break;
        }
        link = strtok(NULL, "/");
    }

    free(cp);
    return check;
}


static int germa_getattr(const char *path, struct stat *stbuf)
{
    char path_new[1080];
    int temp;
    strcpy(path_new, direktori);
    strcat(path_new, path);
    printf("getattr %s\n", path_new);
    temp = lstat(path_new, stbuf);

    if (temp == -1)
        return -errno;

    return 0;
}


static int germa_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);

    printf("read %s\n", path_new);

    int temp = 0;
    int lsa = 0;

    (void)fi;

    lsa = open(path_new, O_RDONLY);

    if (lsa == -1)
        return -errno;

    temp = pread(lsa, buf, size, offset);

    if (temp == -1)
        temp = -errno;

    close(lsa);

    return temp;
}


static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);
    printf("getattr %s\n", path_new);
    printf("readdir %s\n", path_new);
    int temp = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path_new);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        temp = (filler(buf, de->d_name, &st, 0));
        if (temp != 0)
            break;
    }
    closedir(dp);
    return 0;
}


static int germa_mkdir(const char *path, mode_t mode)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);

    if (isRestricted(path_new))
    {
        status_log("FAILED", "MKDIR", path_new, "");
        return -EACCES;
    }

int temp = mkdir(path_new, mode);

if (temp == -1)
{
    status_log("FAILED", "MKDIR", path_new, "");
    return -errno;
}

status_log("SUCCESS", "MKDIR", path_new, "");
return 0;
}

static int germa_rmdir(const char *path)
{
char path_new[1080];
int temp;
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)){
    status_log("FAILED", "RMDIR", path_new, "");
    return -EACCES;
}

temp = rmdir(path_new);
if (temp == -1){
    status_log("FAILED", "RMDIR", path_new, "");
    return -errno;
}
status_log("SUCCESS", "RMDIR", path_new, "");
return 0;
}


static int germa_rename(const char *from, const char *to)
{
int temp;
char before[1080];
strcpy(before, direktori);
strcat(before, from);
char new[1080];
strcpy(new, direktori);
strcat(new, to);

if (isRestricted(before)){
    if (isRestricted(new)){
        status_log("FAILED", "RENAME", before, new);
        return -EACCES;
    }
}

temp = rename(before, new);

if (temp == -1){
    status_log("FAILED", "RENAME", before, new);
    return -errno;
}

status_log("SUCCESS", "RENAME", before, new);
return 0;
}


static int germa_unlink(const char *path)
{
int temp;
char path_new[1080];
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)){
    status_log("FAILED", "RMFILE", path_new, "");
    return -EACCES;
}

temp = unlink(path_new);

if (temp == -1){
    status_log("FAILED", "RMFILE", path_new, "");
    return -errno;
}
status_log("SUCCESS", "RMFILE", path_new, "");

return 0;
}

static int germa_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
char path_new[1080];
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)) {
    status_log("FAILED", "TOUCH", path_new, "");
    return -EACCES;
}

int lsa = creat(path_new, mode);
if (lsa == -1) {
    status_log("FAILED", "TOUCH", path_new, "");
    return -errno;
}

fi->fh = lsa;

status_log("SUCCESS", "TOUCH", path_new, "");

return 0;
}

static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .read = germa_read,
    .readdir = germa_readdir,
    .unlink = germa_unlink,
    .create = germa_create,
    .rename = germa_rename,
    .mkdir = germa_mkdir,
    .rmdir = germa_rmdir,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_oper, NULL);
}

```

# Soal 4
Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut. 
Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Contoh:
REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
FLAG::230419-12:29:33::RMDIR::/bsi23

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.


## Soal 4 bagian a
Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 


## Cara Penyelesaian`
dibuat program untuk menerima path sebuah direktori sebagai input dan memeriksa apakah direktori tersebut merupakan direktori modulardirektori yang memiliki awalan "module_" pada namanya. Program ini menggunakan loop dan fungsi string untuk memeriksa setiap token dari path secara berurutan. Jika token dengan awalan yang sesuai ditemukan, program akan mengembalikan substring path. Jika tidak, program mengembalikan nilai NULL. lalu dibuat fungsi rename pada fuse operations

## Source Code
```sh

char *modular_check(const char *path) {
  char temp[strlen(path) + 1];
  strcpy(temp, path);
  const char *prefix = "module_";
  int prefixLen = 7;

  const char* slas = "/";
  char *tuk = strtok((char *) temp, slas);

  while (tuk != NULL) {
    if (strncmp(tuk, prefix, prefixLen) == 0) {
      int jck = strstr(path, tuk) - path + strlen(tuk);
      char *string_sub = (char *) malloc(jck + 1);
      strncpy(string_sub, path, jck);
      string_sub[jck] = '\0';
      return string_sub;
    }
    tuk = strtok(NULL, slas);
  }
  return NULL;
}


static int xmp_rename(const char *oldpath, const char *newpath) {
  int result = rename(oldpath, newpath);
  if (result == -1) return -errno;

  char *argv[] = {"RENAME", (char *) oldpath, (char *) newpath};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  char *path = modular_check(newpath);
  if (path) {
    modularisasi(newpath);
  }
   else {
    de_modularisasi(newpath);
  }

  return 0;
}


```


## Soal 4 bagian c
Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

## Cara Penyelesaian`
dibuat fungsi untuk mencatat status log ke file fs_module.log dengan format yang telah ditentukan. dengan mengambil data waktu saat ini menggunakan fungsi `time()` dan mengonversinya menjadi waktu lokal menggunakan `localtime()`.Selanjutnya, diperiksa (argv[0]) untuk menentukan tingkat log. Jika argumen tersebut adalah "RMDIR" atau "UNLINK", maka level log ditetapkan sebagai "REPORT". Jika tidak, level log ditetapkan sebagai "FLAG".

Kemudian menggabungkannya dengan string `temp` menggunakan fungsi `sprintf()`. output dari program ini mencakup informasi tingkat log (REPORT atau FLAG), waktu, perintah, dan deskripsi tambahan lainnya.

## Source Code
```sh

void status_log(int argc, char *argv[]) {
  time_t currentTime = time(NULL);
  struct tm *localTime = localtime(&currentTime);
  char level[20];
  char temp[5000];
  sprintf(temp, "%s", argv[0]);

  if (strcmp(temp,"RMDIR") || strcmp(temp,"UNLINK")) {
    sprintf(level,"REPORT");
  }
  else {
    sprintf(level,"FLAG");
  }


  for (int i = 1; i < argc; i++) {
    sprintf(temp + strlen(temp), "::%s",argv[i]);
  }

  int second = localTime->tm_sec;
  int minute = localTime->tm_min;
  int hour = localTime->tm_hour;
  int day = localTime->tm_mday;
  int month = localTime->tm_mon + 1;
  int year = localTime->tm_year % 100;
  fprintf(name_file, "%s::%d%d%d-%02d:%02d:%02d::%s\n", level, day, month, year, hour, minute, second, temp);
  fflush(name_file);
}

```


## Soal 4 bagian b & d
- Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.
- Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).


## Cara Penyelesaian`
Dibuat fungsi untuk membaca setiap entri (file/direktori) di dalamnya, dan memprosesnya satu per satu. Jika entri adalah direktori, maka fungsi akan melakukan rekursi ke dalam direktori tersebut. Jika entri adalah file, fungsi `file_split` akan dipanggil untuk membagi file menjadi file-file kecil berukuran 1024 bytes dan mencatat status log menggunakan `status_log`.

## Source Code
```sh

void modularisasi(const char *path) {
  DIR *dir_file = opendir(path);
  if (dir_file == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir_file)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

      status_log(1, (char*[1]) {filePath});
    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      modularisasi(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      file_split(filePath);
    }
  }
}

#file_split

void file_split(const char *path) {
  FILE *yuks = fopen(path, "rb");
  if (yuks == NULL) return;

  fseek(yuks, 0, SEEK_END);
  long fileSize = ftell(yuks);
  rewind(yuks);

  int numfiles = (fileSize + path_size - 1) / path_size;

  char output[4096];
  FILE *best;
  size_t bytesRead;
  char buffer[path_size];

  for (int i = 0; i < numfiles; i++) {
    sprintf(output, "%s.%03d", path, i);
    best = fopen(output, "wb");
    if (best == NULL) {
      fclose(yuks);
      return;
    }

    bytesRead = fread(buffer, 1, path_size, yuks);
    fwrite(buffer, 1, bytesRead, best);
    fclose(best);
  }
  fclose(yuks);
  remove(path);
}

```


## Soal 4 bagian E
Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).


## Cara Penyelesaian`
melakukan de-modularisasi pada direktori yang diberikan sebagai input. Fungsi ini juga akan membuka direktori tersebut, membaca setiap entri di dalamnya, dan memprosesnya satu per satu. Jika entri adalah direktori, fungsi akan melakukan rekursi ke dalam direktori tersebut. Jika entri adalah file, fungsi akan memeriksa apakah file memiliki ekstensi ".000". Jika iya, fungsi `file_combine` akan dipanggil untuk menggabungkan file-file kecil menjadi file asli dan menghapus file-file kecil yang sudah digabungkan.

## Source Code
```sh
void de_modularisasi(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      de_modularisasi(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      int jck = strlen(filePath);
      if (jck >= 4 && !strcmp(filePath + jck - 4, ".000")) {
        char baseName[256];
        strncpy(baseName, filePath, jck - 4);
        file_combine(baseName);
      }
    }
  }
}


#file combine

void file_combine(const char *path) {
  FILE *best = fopen(path, "wb");
  if (best == NULL) {
    perror("fopen");
    return;
  }

  int no = 0;
  const char *path_file = file_path(path, no++);
  while (access(path_file, F_OK) == 0) {
    FILE *fs = fopen(path_file, "rb");
    if (fs == NULL) {
      fclose(best);
      perror("fopen");
      return;
    }
    
    char buffer[path_size];
    size_t bytesRead = fread(buffer, 1, path_size, fs);

    fwrite(buffer, 1, bytesRead, best);
    
    fclose(fs);

    path_file = file_path(path, no++);
  }
  no = 0;
  path_file = file_path(path, no++);
  while (access(path_file, F_OK) == 0) {
    remove(path_file);
    path_file = file_path(path, no++);
  }

  fclose(best);
}

```
### hasil output
[list output](https://drive.google.com/file/d/184q6LLOx7xLdSRkzPUcgDcsfiQ6o4btt/view?usp=sharing)

