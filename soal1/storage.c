#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

#define MAX_LINE_LENGTH 1024
#define MAX_FIELDS 13

int main() {
    pid_t pid;
    int status;

    pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execlp("kaggle", "kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    }
    waitpid(pid, &status, 0);

    pid = fork();
    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        execlp("unzip", "unzip", "fifa-player-stats-database.zip", NULL);
        perror("execlp");
        exit(EXIT_FAILURE);
    }
    waitpid(pid, &status, 0);


    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        perror("Failed to open the CSV file");
        exit(EXIT_FAILURE);
    }

    char line[MAX_LINE_LENGTH];
    int line_count = 0;
    while (fgets(line, sizeof(line), file)) {
        if (line_count > 0) {
            char *field;
            char *fields[MAX_FIELDS];
            int field_count = 0;

            // Split the line by comma delimiter
            field = strtok(line, ",");
            while (field != NULL && field_count < MAX_FIELDS) {
                fields[field_count++] = field;
                field = strtok(NULL, ",");
            }

            if (field_count >= 12) {
                // Extract relevant fields
                char *name = fields[1];
                char *club = fields[8];
                int age = atoi(fields[2]);
                int potential = atoi(fields[7]);
                char *photo_url = fields[5];
                char *nationality = fields[4];
                char *price = fields[10];
                char *wage = fields[11];

                // Check conditions
                if (age < 25 && potential > 85 && strcmp(club, "Manchester City") != 0) {
                    printf("---------------------------------------\n");
                    printf("Name: %s\n", name);
                    printf("Club: %s\n", club);
                    printf("Age: %d\n", age);
                    printf("Potential: %d\n", potential);
                    printf("Link Foto: %s\n", photo_url);
                    printf("Nationality: %s\n", nationality);
                    printf("Value: %s\n", price);
                    printf("Wage: %s\n", wage);
                    printf("---------------------------------------\n");
                }
            }
        }

        line_count++;
    }

    fclose(file);

    return 0;
}
