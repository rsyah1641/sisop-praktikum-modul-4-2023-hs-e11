#define FUSE_USE_VERSION 28
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fuse.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/time.h>
#define path_size 1024


FILE *name_file;

const char *file_path(const char *path, int no) {
  char *path_file = malloc(strlen(path) + 5);
  sprintf(path_file, "%s.%03d", path, no);
  return path_file;
}


char *modular_check(const char *path) {
  char temp[strlen(path) + 1];
  strcpy(temp, path);
  const char *prefix = "module_";
  int prefixLen = 7;

  const char* slas = "/";
  char *tuk = strtok((char *) temp, slas);

  while (tuk != NULL) {
    if (strncmp(tuk, prefix, prefixLen) == 0) {
      int jck = strstr(path, tuk) - path + strlen(tuk);
      char *string_sub = (char *) malloc(jck + 1);
      strncpy(string_sub, path, jck);
      string_sub[jck] = '\0';
      return string_sub;
    }
    tuk = strtok(NULL, slas);
  }
  return NULL;
}


void file_split(const char *path) {
  FILE *yuks = fopen(path, "rb");
  if (yuks == NULL) return;

  fseek(yuks, 0, SEEK_END);
  long fileSize = ftell(yuks);
  rewind(yuks);

  int numfiles = (fileSize + path_size - 1) / path_size;

  char output[4096];
  FILE *best;
  size_t bytesRead;
  char buffer[path_size];

  for (int i = 0; i < numfiles; i++) {
    sprintf(output, "%s.%03d", path, i);
    best = fopen(output, "wb");
    if (best == NULL) {
      fclose(yuks);
      return;
    }

    bytesRead = fread(buffer, 1, path_size, yuks);
    fwrite(buffer, 1, bytesRead, best);
    fclose(best);
  }
  fclose(yuks);
  remove(path);
}

int check_file_split(const char *path) {
  const char *kom = strrchr(path, '.');
  if (kom == NULL) return 0;
  for (int i = 0; i <= 999; i++) {
    char no[6];
    sprintf(no, ".%.03d", i);
    if (!strcmp(kom, no)) return 1;
  }
  return 0;
}

void status_log(int argc, char *argv[]) {
  time_t currentTime = time(NULL);
  struct tm *localTime = localtime(&currentTime);
  char level[20];
  char temp[5000];
  sprintf(temp, "%s", argv[0]);

  if (strcmp(temp,"RMDIR") || strcmp(temp,"UNLINK")) {
    sprintf(level,"REPORT");
  }
  else {
    sprintf(level,"FLAG");
  }


  for (int i = 1; i < argc; i++) {
    sprintf(temp + strlen(temp), "::%s",argv[i]);
  }

  int second = localTime->tm_sec;
  int minute = localTime->tm_min;
  int hour = localTime->tm_hour;
  int day = localTime->tm_mday;
  int month = localTime->tm_mon + 1;
  int year = localTime->tm_year % 100;
  fprintf(name_file, "%s::%d%d%d-%02d:%02d:%02d::%s\n", level, day, month, year, hour, minute, second, temp);
  fflush(name_file);
}


void file_combine(const char *path) {
  FILE *best = fopen(path, "wb");
  if (best == NULL) {
    perror("fopen");
    return;
  }

  int no = 0;
  const char *path_file = file_path(path, no++);
  while (access(path_file, F_OK) == 0) {
    FILE *fs = fopen(path_file, "rb");
    if (fs == NULL) {
      fclose(best);
      perror("fopen");
      return;
    }
    
    char buffer[path_size];
    size_t bytesRead = fread(buffer, 1, path_size, fs);

    fwrite(buffer, 1, bytesRead, best);
    
    fclose(fs);

    path_file = file_path(path, no++);
  }
  no = 0;
  path_file = file_path(path, no++);
  while (access(path_file, F_OK) == 0) {
    remove(path_file);
    path_file = file_path(path, no++);
  }

  fclose(best);
}

void de_modularisasi(const char *path) {
  DIR *dir = opendir(path);
  if (dir == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      de_modularisasi(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      int jck = strlen(filePath);
      if (jck >= 4 && !strcmp(filePath + jck - 4, ".000")) {
        char baseName[256];
        strncpy(baseName, filePath, jck - 4);
        file_combine(baseName);
      }
    }
  }
}

void modularisasi(const char *path) {
  DIR *dir_file = opendir(path);
  if (dir_file == NULL) {
    perror("dir");
    return;
  }

  struct dirent* entry;

  while ((entry = readdir(dir_file)) != NULL) {
    char filePath[4096];
    sprintf(filePath, "%s/%s", path, entry->d_name);

      status_log(1, (char*[1]) {filePath});
    struct stat st;

    if (stat(filePath, &st) == -1) {
      continue;
    }

    if (S_ISDIR(st.st_mode)) {
      if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

      modularisasi(filePath);   
    } else if (S_ISREG(st.st_mode)) {
      file_split(filePath);
    }
  }
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
  int result = lstat(path,stbuf);
  if (result == -1) return -errno;

  char *argv[] = {"GETATTR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_access(const char *path, int mask) {
  char *argv[] = {"ACCESS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_readlink(const char *path, char *buf, size_t size) {
  char *argv[] = {" LINK", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
  DIR *dp;
  struct dirent *de;
  (void) offset;
  (void) fi;

  dp = opendir(path);

  if (dp == NULL) return -errno;

  while ((de = readdir(dp)) != NULL) {
    size_t jck = strlen(de->d_name);
    if (check_file_split(de->d_name)) {
      if (jck >= 4 && !strcmp(de->d_name + jck - 4, ".000")) {
        char baseName[256];
        char filePath[4096];
        strncpy(baseName, de->d_name, jck - 4);
        baseName[jck - 4] = '\0';
        sprintf(filePath, "%s/%s", path, baseName);
        file_combine(filePath);
      }
    }
    struct stat st;

    memset(&st, 0, sizeof(st));

    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;

    if (filler(buf, de->d_name, &st, 0)) break;
  }
  closedir(dp);

  char *argv[] = {"READDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc,argv);

  return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev) {
  char *argv[] = {"MKNOD", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);
  return 0;
}

static int xmp_symlink(const char *to, const char *from) {
  char *argv[] = {"SYMLINK", (char *) to, (char *) from};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_mkdir(const char *path, mode_t mode) {
  int result = mkdir(path, mode);
  if (result == -1) return -errno;

  char *argv[] = {"MKDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_unlink(const char *path) {
  int result = unlink(path);
  if (result == -1) return -errno;

  char *argv[] = {"UNLINK", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);
  return 0;
}

static int xmp_rmdir(const char *path) {
  int result = rmdir(path);
  if (result == -1) return -errno;

  char *argv[] = {"RMDIR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_rename(const char *oldpath, const char *newpath) {
  int result = rename(oldpath, newpath);
  if (result == -1) return -errno;

  char *argv[] = {"RENAME", (char *) oldpath, (char *) newpath};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  char *path = modular_check(newpath);
  if (path) {
    modularisasi(newpath);
  }
   else {
    de_modularisasi(newpath);
  }

  return 0;
}
static int xmp_link(const char *to, const char *from) {
  char *argv[] = {"LINK", (char *) to, (char *) from};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_chmod(const char *path, mode_t mode) {
  char *argv[] = {"CHMOD", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_chown(const char *path, uid_t uid, gid_t gid) {
  char *argv[] = {"CHOWN", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_truncate(const char *path, off_t size) {
  char *argv[] = {"TRUNCATE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2]) {
  char *argv[] = {"UTIMENS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
  char *argv[] = {"OPEN", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  (void)fi;

  FILE* fp = fopen(path, "r");
  if (fp == NULL) {
    return -errno;
  }
  if (fseek(fp, offset, SEEK_SET) == -1) {
    fclose(fp);
    return -errno;
  }

  size_t bytesRead = fread(buf, 1, size, fp);

  fclose(fp);
  
  char *argv[] = {"READ", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return bytesRead;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  (void)fi;

  FILE *fp = fopen(path, "w+");
  if (fp == NULL) {
    return -errno;
  }

  if (fseek(fp, offset, SEEK_SET) == -1) {
    fclose(fp);
    return -errno;
  }

  if (fwrite(buf, size, 1, fp) != 1) {
    fclose(fp);
    return -errno;
  }

  fclose(fp);

  char *argv[] = {"WRITE", (char *) path, (char *) buf};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return size;
}


static int xmp_statfs(const char *path, struct statvfs *stbuf) {
  char *argv[] = {"STATFS", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
  if (access(path, F_OK) == 0) return -EEXIST;

  int result = creat(path, mode);
  if ( result == -1) return -errno;

  fi->fh = result;

  char *argv[] = {"CREATE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_release(const char *path, struct fuse_file_info *fi) {
  char *argv[] = {"RELEASE", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info* fi) {
  char *argv[] = {"FSYNC", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_setxattr(const char* path, const char* name, const char* value, size_t size, int flags) {
  char *argv[] = {"SETXATTR", (char *) path, (char *) name, (char *) value};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_getxattr(const char* path, const char* name, char* value, size_t size) {
  char *argv[] = {"GETXATTR", (char *) path, (char *) name, (char *) value};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_listxattr(const char* path, char* list, size_t size) {
  char *argv[] = {"LISTXATTR", (char *) path};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

static int xmp_removexattr(const char *path, const char *name) {
  char *argv[] = {"REMOVEXATTR", (char *) path, (char *) name};
  int argc = sizeof(argv) / sizeof(argv[0]);
  status_log(argc, argv);

  return 0;
}

void check_file(){
    char *dir_home = getpwuid(getuid())->pw_dir;
  char logFilePath[500];
  sprintf(logFilePath, "%s/fs_module.log", dir_home);
  name_file = fopen(logFilePath, "w");

  if (name_file == NULL) {
    perror("failed");
    return 1;
  }
}
static struct fuse_operations xmp_operations = {
  .getattr = xmp_getattr,
  .access = xmp_access,
  .readlink = xmp_readlink,
  .readdir = xmp_readdir,
  .mknod = xmp_mknod,
  .rename = xmp_rename,
  .mkdir = xmp_mkdir,
  .rmdir = xmp_rmdir,
  .unlink = xmp_unlink,
  .symlink = xmp_symlink,
  .chown = xmp_chown,
  .link = xmp_link,
  .chmod = xmp_chmod,
  .truncate = xmp_truncate,
  .utimens = xmp_utimens,
  .read = xmp_read,
  .open = xmp_open,
  .write = xmp_write,
  .statfs = xmp_statfs,
  .release = xmp_release,
  .create = xmp_create,
  .fsync = xmp_fsync,
  .getxattr = xmp_getxattr,
  .setxattr = xmp_setxattr,
  .listxattr = xmp_listxattr,
  .removexattr = xmp_removexattr,
};



int main(int  argc, char *argv[]) {
  umask(0);
 check_file();
  return fuse_main(argc, argv, &xmp_operations, NULL);
}
