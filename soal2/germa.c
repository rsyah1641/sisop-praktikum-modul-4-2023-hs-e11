#define FUSE_USE_VERSION 30
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/time.h>
#include <dirent.h>
#include <unistd.h>
#include <fuse.h>
#include <errno.h>

void status_log(const char *temp, const char *operation, const char *desc, const char *add)
{
time_t now;
time(&now);
struct tm *timestamp = localtime(&now);
FILE *logging = fopen("/home/riansyah/praktikum4/soal2/logmucatatsini.txt", "a");

char clock[20];
strftime(clock, sizeof(clock), "%d/%m/%Y-%H:%M:%S", timestamp);

if (logging != NULL) {
    fprintf(logging, "%s::%s::%s::riansyah-", temp, clock, operation);

            if (strcmp(operation, "MKDIR") == 0) {
            fprintf(logging, "Create directory %s\n", desc);
        } else if (strcmp(operation, "RMDIR") == 0) {
            fprintf(logging, "Remove directory %s\n", desc);
        } else if (strcmp(operation, "RENAME") == 0) {
            fprintf(logging, "Rename from %s to %s\n", desc,add);
        } else if (strcmp(operation, "TOUCH") == 0) {
            fprintf(logging, "Created file %s\n", desc);
        }else if (strcmp(operation, "RMFILE") == 0) {
            fprintf(logging, "Remove file %s\n", desc);
        } 

        fclose(logging);
    }
}

const char *direktori = "/home/riansyah/praktikum4/soal2/nanaxgerma/src_data";

static bool isRestricted(const char *path) {
    bool check = false;
    char *cp = strdup(path);
    char *link = strtok(cp, "/");

    while (link != NULL) {
        if (strstr(link, "bypass") != NULL) {
            check = false;
            break;
        }
        if (strstr(link, "restricted") != NULL) {
            check = true;
            break;
        }
        link = strtok(NULL, "/");
    }

    free(cp);
    return check;
}


static int germa_getattr(const char *path, struct stat *stbuf)
{
    char path_new[1080];
    int temp;
    strcpy(path_new, direktori);
    strcat(path_new, path);
    printf("getattr %s\n", path_new);
    temp = lstat(path_new, stbuf);

    if (temp == -1)
        return -errno;

    return 0;
}


static int germa_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);

    printf("read %s\n", path_new);

    int temp = 0;
    int lsa = 0;

    (void)fi;

    lsa = open(path_new, O_RDONLY);

    if (lsa == -1)
        return -errno;

    temp = pread(lsa, buf, size, offset);

    if (temp == -1)
        temp = -errno;

    close(lsa);

    return temp;
}


static int germa_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);
    printf("getattr %s\n", path_new);
    printf("readdir %s\n", path_new);
    int temp = 0;
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(path_new);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        temp = (filler(buf, de->d_name, &st, 0));
        if (temp != 0)
            break;
    }
    closedir(dp);
    return 0;
}


static int germa_mkdir(const char *path, mode_t mode)
{
    char path_new[1080];
    strcpy(path_new, direktori);
    strcat(path_new, path);

    if (isRestricted(path_new))
    {
        status_log("FAILED", "MKDIR", path_new, "");
        return -EACCES;
    }

int temp = mkdir(path_new, mode);

if (temp == -1)
{
    status_log("FAILED", "MKDIR", path_new, "");
    return -errno;
}

status_log("SUCCESS", "MKDIR", path_new, "");
return 0;
}

static int germa_rmdir(const char *path)
{
char path_new[1080];
int temp;
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)){
    status_log("FAILED", "RMDIR", path_new, "");
    return -EACCES;
}

temp = rmdir(path_new);
if (temp == -1){
    status_log("FAILED", "RMDIR", path_new, "");
    return -errno;
}
status_log("SUCCESS", "RMDIR", path_new, "");
return 0;
}


static int germa_rename(const char *from, const char *to)
{
int temp;
char before[1080];
strcpy(before, direktori);
strcat(before, from);
char new[1080];
strcpy(new, direktori);
strcat(new, to);

if (isRestricted(before)){
    if (isRestricted(new)){
        status_log("FAILED", "RENAME", before, new);
        return -EACCES;
    }
}

temp = rename(before, new);

if (temp == -1){
    status_log("FAILED", "RENAME", before, new);
    return -errno;
}

status_log("SUCCESS", "RENAME", before, new);
return 0;
}


static int germa_unlink(const char *path)
{
int temp;
char path_new[1080];
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)){
    status_log("FAILED", "RMFILE", path_new, "");
    return -EACCES;
}

temp = unlink(path_new);

if (temp == -1){
    status_log("FAILED", "RMFILE", path_new, "");
    return -errno;
}
status_log("SUCCESS", "RMFILE", path_new, "");

return 0;
}

static int germa_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
char path_new[1080];
strcpy(path_new, direktori);
strcat(path_new, path);
if (isRestricted(path_new)) {
    status_log("FAILED", "TOUCH", path_new, "");
    return -EACCES;
}

int lsa = creat(path_new, mode);
if (lsa == -1) {
    status_log("FAILED", "TOUCH", path_new, "");
    return -errno;
}

fi->fh = lsa;

status_log("SUCCESS", "TOUCH", path_new, "");

return 0;
}

static struct fuse_operations germa_oper = {
    .getattr = germa_getattr,
    .read = germa_read,
    .readdir = germa_readdir,
    .unlink = germa_unlink,
    .create = germa_create,
    .rename = germa_rename,
    .mkdir = germa_mkdir,
    .rmdir = germa_rmdir,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &germa_oper, NULL);
}

